package com.bjsxt.mappers;

import com.bjsxt.domain.Market;
import com.bjsxt.domain.TurnoverOrder;
import com.bjsxt.dto.TradeMarketDto;
import com.bjsxt.dto.TurnoverOrderDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TurnoverOrderDtoMappers {
    TurnoverOrderDtoMappers INSTANCE = Mappers.getMapper(TurnoverOrderDtoMappers.class) ;

    TurnoverOrder toConvertEntity(TurnoverOrderDto source) ;


    TurnoverOrderDto toConvertDto(TurnoverOrder source) ;


    List<TurnoverOrder> toConvertEntity(List<TurnoverOrderDto> source) ;


    List<TurnoverOrderDto> toConvertDto(List<TurnoverOrder> source) ;
}
