package com.bjsxt.mappers;

import com.bjsxt.domain.Market;

import com.bjsxt.dto.TradeMarketDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(componentModel = "spring")
public interface TradeMarketDtoMappers {

    TradeMarketDtoMappers INSTANCE = Mappers.getMapper(TradeMarketDtoMappers.class) ;

    Market toConvertEntity(TradeMarketDto source) ;


    TradeMarketDto toConvertDto(Market source) ;


    List<Market> toConvertEntity(List<TradeMarketDto> source) ;


    List<TradeMarketDto> toConvertDto(List<Market> source) ;
}
