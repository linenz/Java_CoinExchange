package com.bjsxt.mappers;

import com.bjsxt.domain.Market;
import com.bjsxt.dto.TradeMarketDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-30T22:41:46+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_351 (Oracle Corporation)"
)
@Component
public class TradeMarketDtoMappersImpl implements TradeMarketDtoMappers {

    @Override
    public Market toConvertEntity(TradeMarketDto source) {
        if ( source == null ) {
            return null;
        }

        Market market = new Market();

        market.setSymbol( source.getSymbol() );
        market.setName( source.getName() );
        market.setNumMin( source.getNumMin() );
        market.setNumMax( source.getNumMax() );
        market.setTradeMin( source.getTradeMin() );
        market.setTradeMax( source.getTradeMax() );
        market.setPriceScale( (byte) source.getPriceScale() );
        market.setNumScale( (byte) source.getNumScale() );
        market.setMergeDepth( source.getMergeDepth() );
        market.setSort( source.getSort() );

        return market;
    }

    @Override
    public TradeMarketDto toConvertDto(Market source) {
        if ( source == null ) {
            return null;
        }

        TradeMarketDto tradeMarketDto = new TradeMarketDto();

        tradeMarketDto.setSymbol( source.getSymbol() );
        tradeMarketDto.setName( source.getName() );
        if ( source.getPriceScale() != null ) {
            tradeMarketDto.setPriceScale( source.getPriceScale() );
        }
        if ( source.getNumScale() != null ) {
            tradeMarketDto.setNumScale( source.getNumScale() );
        }
        tradeMarketDto.setNumMin( source.getNumMin() );
        tradeMarketDto.setNumMax( source.getNumMax() );
        tradeMarketDto.setTradeMin( source.getTradeMin() );
        tradeMarketDto.setTradeMax( source.getTradeMax() );
        tradeMarketDto.setMergeDepth( source.getMergeDepth() );
        if ( source.getSort() != null ) {
            tradeMarketDto.setSort( source.getSort() );
        }

        return tradeMarketDto;
    }

    @Override
    public List<Market> toConvertEntity(List<TradeMarketDto> source) {
        if ( source == null ) {
            return null;
        }

        List<Market> list = new ArrayList<Market>( source.size() );
        for ( TradeMarketDto tradeMarketDto : source ) {
            list.add( toConvertEntity( tradeMarketDto ) );
        }

        return list;
    }

    @Override
    public List<TradeMarketDto> toConvertDto(List<Market> source) {
        if ( source == null ) {
            return null;
        }

        List<TradeMarketDto> list = new ArrayList<TradeMarketDto>( source.size() );
        for ( Market market : source ) {
            list.add( toConvertDto( market ) );
        }

        return list;
    }
}
