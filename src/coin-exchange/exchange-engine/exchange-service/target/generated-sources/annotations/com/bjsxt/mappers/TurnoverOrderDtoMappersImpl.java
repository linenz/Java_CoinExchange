package com.bjsxt.mappers;

import com.bjsxt.domain.TurnoverOrder;
import com.bjsxt.dto.TurnoverOrderDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-30T22:41:46+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_351 (Oracle Corporation)"
)
@Component
public class TurnoverOrderDtoMappersImpl implements TurnoverOrderDtoMappers {

    @Override
    public TurnoverOrder toConvertEntity(TurnoverOrderDto source) {
        if ( source == null ) {
            return null;
        }

        TurnoverOrder turnoverOrder = new TurnoverOrder();

        turnoverOrder.setId( source.getId() );
        turnoverOrder.setMarketId( source.getMarketId() );
        turnoverOrder.setMarketType( source.getMarketType() );
        turnoverOrder.setTradeType( source.getTradeType() );
        turnoverOrder.setSymbol( source.getSymbol() );
        turnoverOrder.setMarketName( source.getMarketName() );
        turnoverOrder.setSellUserId( source.getSellUserId() );
        turnoverOrder.setSellCoinId( source.getSellCoinId() );
        turnoverOrder.setSellOrderId( source.getSellOrderId() );
        turnoverOrder.setSellPrice( source.getSellPrice() );
        turnoverOrder.setSellFeeRate( source.getSellFeeRate() );
        turnoverOrder.setSellVolume( source.getSellVolume() );
        turnoverOrder.setBuyUserId( source.getBuyUserId() );
        turnoverOrder.setBuyCoinId( source.getBuyCoinId() );
        turnoverOrder.setBuyOrderId( source.getBuyOrderId() );
        turnoverOrder.setBuyVolume( source.getBuyVolume() );
        turnoverOrder.setBuyPrice( source.getBuyPrice() );
        turnoverOrder.setBuyFeeRate( source.getBuyFeeRate() );
        turnoverOrder.setOrderId( source.getOrderId() );
        turnoverOrder.setAmount( source.getAmount() );
        turnoverOrder.setPrice( source.getPrice() );
        turnoverOrder.setVolume( source.getVolume() );
        turnoverOrder.setDealSellFee( source.getDealSellFee() );
        turnoverOrder.setDealSellFeeRate( source.getDealSellFeeRate() );
        turnoverOrder.setDealBuyFee( source.getDealBuyFee() );
        turnoverOrder.setDealBuyFeeRate( source.getDealBuyFeeRate() );
        turnoverOrder.setStatus( source.getStatus() );
        turnoverOrder.setLastUpdateTime( source.getLastUpdateTime() );
        turnoverOrder.setCreated( source.getCreated() );

        return turnoverOrder;
    }

    @Override
    public TurnoverOrderDto toConvertDto(TurnoverOrder source) {
        if ( source == null ) {
            return null;
        }

        TurnoverOrderDto turnoverOrderDto = new TurnoverOrderDto();

        turnoverOrderDto.setId( source.getId() );
        turnoverOrderDto.setMarketId( source.getMarketId() );
        turnoverOrderDto.setMarketType( source.getMarketType() );
        turnoverOrderDto.setTradeType( source.getTradeType() );
        turnoverOrderDto.setSymbol( source.getSymbol() );
        turnoverOrderDto.setMarketName( source.getMarketName() );
        turnoverOrderDto.setSellUserId( source.getSellUserId() );
        turnoverOrderDto.setSellCoinId( source.getSellCoinId() );
        turnoverOrderDto.setSellOrderId( source.getSellOrderId() );
        turnoverOrderDto.setSellPrice( source.getSellPrice() );
        turnoverOrderDto.setSellFeeRate( source.getSellFeeRate() );
        turnoverOrderDto.setSellVolume( source.getSellVolume() );
        turnoverOrderDto.setBuyUserId( source.getBuyUserId() );
        turnoverOrderDto.setBuyCoinId( source.getBuyCoinId() );
        turnoverOrderDto.setBuyOrderId( source.getBuyOrderId() );
        turnoverOrderDto.setBuyVolume( source.getBuyVolume() );
        turnoverOrderDto.setBuyPrice( source.getBuyPrice() );
        turnoverOrderDto.setBuyFeeRate( source.getBuyFeeRate() );
        turnoverOrderDto.setOrderId( source.getOrderId() );
        turnoverOrderDto.setAmount( source.getAmount() );
        turnoverOrderDto.setPrice( source.getPrice() );
        turnoverOrderDto.setVolume( source.getVolume() );
        turnoverOrderDto.setDealSellFee( source.getDealSellFee() );
        turnoverOrderDto.setDealSellFeeRate( source.getDealSellFeeRate() );
        turnoverOrderDto.setDealBuyFee( source.getDealBuyFee() );
        turnoverOrderDto.setDealBuyFeeRate( source.getDealBuyFeeRate() );
        turnoverOrderDto.setStatus( source.getStatus() );
        turnoverOrderDto.setLastUpdateTime( source.getLastUpdateTime() );
        turnoverOrderDto.setCreated( source.getCreated() );

        return turnoverOrderDto;
    }

    @Override
    public List<TurnoverOrder> toConvertEntity(List<TurnoverOrderDto> source) {
        if ( source == null ) {
            return null;
        }

        List<TurnoverOrder> list = new ArrayList<TurnoverOrder>( source.size() );
        for ( TurnoverOrderDto turnoverOrderDto : source ) {
            list.add( toConvertEntity( turnoverOrderDto ) );
        }

        return list;
    }

    @Override
    public List<TurnoverOrderDto> toConvertDto(List<TurnoverOrder> source) {
        if ( source == null ) {
            return null;
        }

        List<TurnoverOrderDto> list = new ArrayList<TurnoverOrderDto>( source.size() );
        for ( TurnoverOrder turnoverOrder : source ) {
            list.add( toConvertDto( turnoverOrder ) );
        }

        return list;
    }
}
